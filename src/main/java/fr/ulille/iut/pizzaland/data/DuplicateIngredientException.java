package fr.ulille.iut.pizzaland.data;

public class DuplicateIngredientException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 3911842939803684452L;

    public DuplicateIngredientException(String name) {
        super("L'ingredient existe déjà : " + name);
    }    
}