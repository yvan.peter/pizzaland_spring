package fr.ulille.iut.pizzaland;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("fr.ulille.iut.pizzaland")
public class AppConfig {}