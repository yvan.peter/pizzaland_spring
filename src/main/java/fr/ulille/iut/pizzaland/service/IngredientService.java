package fr.ulille.iut.pizzaland.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import fr.ulille.iut.pizzaland.data.DuplicateIngredientException;
import fr.ulille.iut.pizzaland.data.IngredientRepository;
import fr.ulille.iut.pizzaland.representation.Ingredient;

@Service
public class IngredientService {
    private IngredientRepository ingredientRepository;

    @Autowired
    public IngredientService(final IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public List<Ingredient> getAll() {
        return ingredientRepository.findAll();
    }

    public Optional<Ingredient> getOne(final long id) {
        return ingredientRepository.findById(id);
    }

    public Ingredient createIngredient(final Ingredient ingredient) {
        try {
            return ingredientRepository.saveAndFlush(ingredient);
        }
        catch ( DataIntegrityViolationException ex ) {
            throw new DuplicateIngredientException(ingredient.getName());
        }
    }

	public void delete(long id) {
        ingredientRepository.deleteById(id);
	}
}