package fr.ulille.iut.pizzaland;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


import static org.hamcrest.Matchers.is;

import fr.ulille.iut.pizzaland.data.IngredientRepository;
import fr.ulille.iut.pizzaland.representation.Ingredient;

@SpringBootTest
@AutoConfigureMockMvc
public class IngredientTests {
    @Autowired
    MockMvc mvc;

    @Autowired
    private IngredientRepository ingredients;
   

    @Test
	public void atStartup_shouldGetemptyList() throws Exception {
       
        mvc.perform(get("/ingredients"))
            .andExpect(status().isOk())
            .andExpect(content().json("[]"));
    }
    
    @Test
    public void withExistingIngredient_shouldGetIngredientWithId() throws Exception {
        Ingredient test = new Ingredient("mozzarella");
        Long id = ingredients.saveAndFlush(test).getId();
        
        mvc.perform(get("/ingredients/" + id))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.name", is("mozzarella")));

        ingredients.deleteAll();
    }
    
}